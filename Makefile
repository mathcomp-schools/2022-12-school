all:
	gem2.7 install --user-install bundler
	~/.gem/ruby/2.7.0/bin/bundle config set --local path 'vendor/bundle'
	~/.gem/ruby/2.7.0/bin/bundle install
	~/.gem/ruby/2.7.0/bin/bundle exec jekyll serve -w
