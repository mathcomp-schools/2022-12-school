---
layout: page
title: Mathematical Components School and Workshop 2022
nav_title: Home
permalink: /
subtitle: foo
nav: false
# profile:
#   align: right
#   image: prof_pic.jpg
#   image_cicular: false # crops the image to make it circular
#   address: >
#     <p>555 your office number</p>
#     <p>123 your address street</p>
#     <p>Your City, State 12345</p>

news: false  # includes a list of news items
selected_papers: false # includes a list of papers marked as "selected={true}"
social: false  # includes social icons at the bottom of the page
---

The Mathematical Components [school](school) will take place on Monday 5th December 2022
to Friday 9th. The school will be in English and will target master or PhD students
with basic knowledge of Coq.
The course will introduce the SSReflect proof language and the
Mathematical Components library, in particular its key principles and tools.

On Wednesday of the same week we are organizing the [workshop](workshop)
“Mathematical Components - 10 years after the Odd Order Theorem”.

Although the workshop is part of the program of the school, it will be a
standalone event not restricted to the participants to the school.

Both events will be held *in person* (pandemic permitting of course).
Both events are free, no subscription fees, but *registration is mandatory*.
Please email Enrico Tassi to register (subject "MC2022").
The deadline for registering is Thursday 24 November.

Organizers:
- Assia Mahboubi (workshop chair) and
- Enrico Tassi (school chair and local organizer)