---
layout: page
title: Venue
permalink: /venue
#subtitle: <a href='#'>Affiliations</a>. Address. Contacts. Moto. Etc.
nav: true
nav_order: 3
# profile:
#   align: right
#   image: prof_pic.jpg
#   image_cicular: false # crops the image to make it circular
#   address: >
#     <p>555 your office number</p>
#     <p>123 your address street</p>
#     <p>Your City, State 12345</p>

news: false  # includes a list of news items
selected_papers: false # includes a list of papers marked as "selected={true}"
social: false  # includes social icons at the bottom of the page
---

The school and the workshop will be held in the Kahn building in the
Inria center of Sophia-Antipolis:

![Inria center of Sophia-Antipolis](assets/img/inria-sophia-antipolis.jpg){:class="img-responsive"}

On [Google maps](https://www.google.com/maps?q=place_id:ChIJy-jkKxkrzBIRsfkePsGuVuY).

## How to get to the Inria center

These are the official [instructions](https://www.inria.fr/en/how-get-inria-centre-universite-cote-dazur-and-its-montpellier-antenna)
to get to the Inria center.

In short, the closest airport is [NCE](https://www.nice.aeroport.fr/en)
and the best bus is [230](https://bus230.github.io/). The closest train station is [Antibes](https://www.garesetconnexions.sncf/fr/gare/frxat/antibes), which is also a [bus hub](https://www.envibus.fr/en.html).

## Where to stay

Three options:
- Sophia-Antipolis is a technology park, busy during the day, sleepy at night.
  A few hotels are close to the Inria center, [one](https://www.marriott.fr/hotels/travel/nceoa-moxy-sophia-antipolis/) is just in front. A few dining options are available at [walking distance](https://www.google.com/maps/search/Restaurants+st+philippe+sophia+antipolis+france/@43.6195445,7.0650747,16z/data=!3m1!4b1).
- [Antibes](https://www.antibes-juanlespins.com/) is a small picturesque
  touristic city, the closest one to the Inria center. Plenty of hotels
  and restaurants. Here a [hotel](https://www.relaisdupostillon.com/en/) often chosen by Inria agents visiting. Connected via bus [A](https://maps.mybus.io/antibes/voyager/lignes/antibes-les-pins-gr-valbonne-sophia-antipolis/) (stop "Inria")
- [Nice](http://www.nice.fr/fr/) is a beautiful city, plenty of options, but a bit
  far away. Consider at least 45 minutes of trip by bus [230](https://bus230.github.io/).

## Cafeteria & restaurants

For lunch one has two options:
- the cafeteria offers good meals for about 10 euros
- a few restaurants are at [walking distance](https://www.google.com/maps/search/Restaurants+st+philippe+sophia+antipolis+france/@43.6195445,7.0650747,16z/data=!3m1!4b1)
