---
layout: page
title: School
permalink: /school
#subtitle: <a href='#'>Affiliations</a>. Address. Contacts. Moto. Etc.
nav: true
# profile:
#   align: right
#   image: prof_pic.jpg
#   image_cicular: false # crops the image to make it circular
#   address: >
#     <p>555 your office number</p>
#     <p>123 your address street</p>
#     <p>Your City, State 12345</p>

news: false  # includes a list of news items
selected_papers: false # includes a list of papers marked as "selected={true}"
social: false  # includes social icons at the bottom of the page
---

The Mathematical Components school will take place on Monday 5th December 2022 to Friday 9th. 

### Flash info:
- Link to [installers](https://www-sop.inria.fr/teams/marelle/MC-2022-installers/)
- Video of the first lecture is [online](https://www-sop.inria.fr/teams/marelle/MC-2022-installers/video1.mp4)
- Video of the fourth lecture is [online](https://www-sop.inria.fr/teams/marelle/MC-2022-installers/video4.mp4)

### 5th

- 09:00 - 10:00 **introduction to SSReflect and the MathComp library** (Enrico Tassi)<br/>
  Tactics, notations, principles of Small Scale Reflection and the
  Mathematical Components library<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson1.html) |
  [lesson1.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson1.v) | 
  [video](https://www-sop.inria.fr/teams/marelle/MC-2022-installers/video1.mp4)
- 10:30 - 12:00 exercise session 1<br/>
  [SSR cheat sheet](https://www-sop.inria.fr/teams/marelle/MC-2022/cheatsheet.pdf) |
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise1.html) |
  [exercise1.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise1_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise1.v)
- 12:00 - 14:00 lunch
- 14:00 - 15:00 **arithmetics** (Laurent Thery)<br/>
  Order, division, primality, permutations<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson2.html) |
  [lesson2.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson2.v)
- 15:30 - 17:00 exercise session 2<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise2.html) |
  [exercise2.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise2_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise2.v)

### 6th

- 09:00 - 10:00 **finite types and big operators** (Yves Bertot)<br/>
  Handling notions of finiteness and finitely iterated operations<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson3.html) |
  [lesson3.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson3.v)
- 10:30 - 12:00 exercise session 3<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise3.html) |
  [exercise3.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise3_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise3.v)

- 12:00 - 14:00 lunch
- 14:00 - 15:00 **SSReflect and its idioms** (Enrico Tassi)<br/>
  Advanced features of the proof language<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson4.html) |
  [lesson4.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson4.v) |
  [video](https://www-sop.inria.fr/teams/marelle/MC-2022-installers/video4.mp4)
- 15:30 - 17:00 exercise session 4<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise4.html) |
  [exercise4.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise4_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise4.v)

### Workshop

See the [dedicated page](workshop)

### 8th

- 09:00 - 10:00 **the algebra library** (Cyril Cohen)<br/>
  Interfaces and instances of the algebra library; notations and theorems about rings
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson5.html) |
  [lesson5.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson5.v)
- 10:30 - 12:00 exercise session 5<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise5.html) |
  [exercise5.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise5_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise5.v)
- 12:00 - 14:00 lunch
- 14:00 - 15:00 **polynomials** (Laurence Rideau)<br/>
  Polynomials: data structure, algorithms and proofs<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson6.html) |
  [lesson6.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson6.v)
- 15:30 - 17:00 exercise session 6<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise6.html) |
  [exercise6.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise6_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise6.v)

### 9th

- 09:00 - 10:00 **matrix library** (Cyril Cohen)<br/>
  Matrices and related algorithms<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson7.html) |
  [lesson7.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson7.v)
- 10:30 - 12:00 exercise session 7<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise7.html) |
  [exercise7.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise7_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise7.v)
- 12:00 - 14:00 lunch
- 14:00 - 15:00 **tooling and automation** (Kazuhiko Sakaguchi)<br/>
  Reflection based automation<br/>
  [online lesson](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson8.html) |
  [lesson8.v](https://www-sop.inria.fr/teams/marelle/MC-2022/lesson8.v)
- 15:30 - 17:00 exercise session 8<br/>
  [exercise online](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise8.html) |
  [exercise8.v](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise8_todo.v) |
  [solutions](https://www-sop.inria.fr/teams/marelle/MC-2022/exercise8.v)
